package main

import (
	"strings"
	"testing"
)

var testCaseString = `
[[TestCase]]
  Description = "тестирование магазина"
  [TestCase.Requests]
    [TestCase.Requests.1]
      URL = "http://httpbin.org/get"
      Method = "GET"
      RequestBody = "{"result": "true"}"
      Cookie = "yummy_cookie=choco; tasty_cookie=strawberry"
      CustomHeaders = "Auth: Token 1111; Auth2: Token 1111"
      NeedSave = true
      Code = 200
      Body = "72aa74a069b2c1a3"
      Time = 435324611
      IgnoreFields = ["headers.Auth"]
      Errors = []
    [TestCase.Requests.2]
      URL = "http://httpbin.org/get"
      Method = "GET"
      RequestBody = "{"result": "true"}"
      Cookie = "yummy_cookie=choco; tasty_cookie=strawberry"
      CustomHeaders = "Auth: Token 1111; Auth2: Token 1111"
      NeedSave = true
      Code = 200
      Body = "72aa74a069b2c1a3"
      Time = 435324611
      IgnoreFields = ["headers.Cookie"]
      Errors = []
`

func TestGetTestCases(t *testing.T) {
	// test valid
	reader := strings.NewReader(testCaseString)
	testCases, _ := getTestCases(reader)
	for _, testCase := range testCases {
		for _, request := range testCase.Requests {
			if request.URL != "http://httpbin.org/get" {
				t.Error("Url not valid in test case")
			}
			if request.Method != "GET" {
				t.Error("Method not valid in test case")
			}
			if request.RequestBody != `{"result": "true"}` {
				t.Error("Body not valid in test case")
			}
			if request.NeedSave != true {
				t.Error("NeedSave not valid in test case")
			}
			if request.Code != 200 {
				t.Error("Code not valid in test case")
			}
			if request.Body != `72aa74a069b2c1a3` {
				t.Error("Body not valid in test case")
			}
			if request.Time != 435324611 {
				t.Error("Time not valid in test case")
			}
		}
	}

	// test invalid toml format
	reader = strings.NewReader("not valid toml")
	_, err := getTestCases(reader)
	if err == nil {
		t.Error("Not return error with not valid reader")
	}
}
