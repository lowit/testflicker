// +build

package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"github.com/valyala/fasthttp"
	"log"
	"strconv"
	"time"
)

func logError(message string, error error) {
	if error != nil {
		log.Fatalf(message+": %s", error)
	}
}

func main() {
	timeSleep := flag.Int(
		"time-sleep", 0, "time sleep between requests")
	// This function will be called by the server for each incoming request.
	//
	// RequestCtx provides a lot of functionality related to http request
	// processing. See RequestCtx docs for details.
	requestHandler := func(ctx *fasthttp.RequestCtx) {
		reqMethod := string(ctx.Method())

		var requestBody map[string]interface{}

		switch reqMethod {
		case "GET", "DELETE":
			responseCode := ctx.QueryArgs().Peek("code")
			responseBody := ctx.QueryArgs().Peek("body")

			// Convert requestCode from []byte to int
			statusCode, err := strconv.Atoi(string(responseCode))
			logError("Error convert", err)

			ctx.SetStatusCode(statusCode)
			ctx.SetBody(responseBody)
		default:
			body := ctx.Request.Body()
			err := json.Unmarshal(body, &requestBody)
			logError("Error unmarshal", err)

			requestCode := requestBody["code"]
			requestBody := requestBody["body"]

			responseCodeJSON, err := json.Marshal(requestCode)
			logError("Error convert response code", err)
			responseBodyJSON, err := json.Marshal(requestBody)
			logError("Error convert response body", err)

			responseCode, err := strconv.Atoi(string(bytes.Trim(responseCodeJSON, "\"")))
			logError("Error convert response code", err)
			responseBody := bytes.Trim(responseBodyJSON, "\"")
			logError("Error convert response body", err)

			ctx.SetStatusCode(responseCode)
			ctx.SetBody(responseBody)
		}

		time.Sleep(time.Millisecond * time.Duration(*timeSleep))
	}

	// Create custom server.
	s := &fasthttp.Server{
		Handler: requestHandler,

		// Every response will contain 'Server: My super server' header.
		Name: "test server for testing of testflicker",

		// Other Server settings may be set here.
	}

	HOST := "127.0.0.1:8080"
	fmt.Println("Start server listening on " + HOST)
	fmt.Println("Enter Ctr+C for exit")
	// Start the server listening for incoming requests on the given address.
	//
	// ListenAndServe returns only on error, so usually it blocks forever.
	if err := s.ListenAndServe(HOST); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}

}
