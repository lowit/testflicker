package main

import (
	"fmt"
	"github.com/valyala/fasthttp"
	"log"
	"math/rand"
	"time"
)

var countRequests = 0

func main() {
	// This function will be called by the server for each incoming request.
	//
	// RequestCtx provides a lot of functionality related to http request
	// processing. See RequestCtx docs for details.
	requestHandler := func(ctx *fasthttp.RequestCtx) {
		countRequests++
		timeCurrent := time.Now().Format(time.StampMicro)
		print(countRequests, ": ", timeCurrent, "\n")
		time.Sleep(time.Millisecond * time.Duration(rand.Intn(500)))
		_, err := fmt.Fprintf(ctx, "%d: %s", countRequests, timeCurrent)
		if err != nil {
			log.Fatalf("error in response: %s", err)
		}
	}

	// Create custom server.
	s := &fasthttp.Server{
		Handler: requestHandler,

		// Every response will contain 'Server: My super server' header.
		Name: "test server for testflicker",

		// Other Server settings may be set here.
	}

	HOST := "127.0.0.1:8080"
	fmt.Println("Start server listening on " + HOST)
	fmt.Println("Enter Ctr+C for exit")
	// Start the server listening for incoming requests on the given address.
	//
	// ListenAndServe returns only on error, so usually it blocks forever.
	if err := s.ListenAndServe(HOST); err != nil {
		log.Fatalf("error in ListenAndServe: %s", err)
	}

}
