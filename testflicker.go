package main

import (
	"errors"
	"flag"
	"fmt"
	"github.com/BurntSushi/toml"
	"github.com/buger/jsonparser"
	"github.com/cespare/xxhash"
	"github.com/valyala/fasthttp"
	"io"
	"os"
	"regexp"
	"strings"
	"time"
)

// TestRequestT description for requests in test cases
type TestRequestT struct {
	// Address API.
	URL string
	// Method API.
	Method string
	// Body for send to API.
	RequestBody string
	// Cookie for send to API.
	Cookie string
	// Custom headers for request.
	CustomHeaders string
	// Flag. If true save response code, body and time for future tests.
	NeedSave bool
	// Expected code for test case.
	Code int
	// Raw response body.
	RawBody []byte
	// Expected body for test case.
	Body string
	// Expected response rime for test case.
	Time int
	// Fields whose values we want to ignore in response.
	IgnoreFields []string
	// Errors of request
	Errors []error
}

// TestCaseGroupT base type for test case
type TestCaseGroupT struct {
	// Description of TestCase
	Description string
	// Requests of TestCase
	Requests map[string]TestRequestT
}

type testCasesT struct {
	TestCase []TestCaseGroupT
}

//	Get test cases from toml file
func getTestCases(file io.Reader) ([]TestCaseGroupT, error) {
	var TestCases testCasesT
	_, err := toml.DecodeReader(file, &TestCases)
	return TestCases.TestCase, err
}

// Do http request for test case
func getTestCaseResponse(request *TestRequestT) (*fasthttp.Response, int, error) {
	req := fasthttp.AcquireRequest()
	req.SetRequestURI(request.URL)
	req.Header.SetMethod(request.Method)
	req.SetBodyString(request.RequestBody)

	// Add cookie
	parsedCookies := strings.Split(request.Cookie, "; ")
	for _, Cookie := range parsedCookies {
		parsedCookie := strings.Split(Cookie, "=")
		if len(parsedCookie) != 2 {
			continue
		}
		keyCookie, valueCookie := parsedCookie[0], parsedCookie[1]
		req.Header.SetCookie(keyCookie, valueCookie)
	}

	// Add custom headers
	parsedHeaders := strings.Split(request.CustomHeaders, "; ")
	for _, Header := range parsedHeaders {
		parsedHeader := strings.Split(Header, ": ")
		if len(parsedHeader) != 2 {
			continue
		}
		HeaderName, HeaderValue := parsedHeader[0], parsedHeader[1]
		req.Header.Add(HeaderName, HeaderValue)
	}

	resp := fasthttp.AcquireResponse()
	client := &fasthttp.Client{}

	timeStart := time.Now().UnixNano()
	err := client.Do(req, resp)
	respTime := int(time.Now().UnixNano() - timeStart)

	return resp, respTime, err
}

// Find and return all params in string
func findParams(url string) []string {
	matcher, _ := regexp.Compile("{.+?}")
	return matcher.FindAllString(url, -1)
}

// Replace all params in template from response data
func replaceParams(url string, rawBody []byte) string {
	for _, token := range findParams(url) {
		value, _, _, _ := jsonparser.Get(rawBody, strings.Trim(token, "{}"))
		url = strings.ReplaceAll(url, token, string(value))
	}
	return url
}

// Get group of sequential requests and iterate over the array
func doTestCaseGroup(testCaseGroup *TestCaseGroupT,
	channel chan TestCaseGroupT,
	update, forceUpdate bool) {

	var firstBody []byte
	var requestURLOriginal string
	responses := make(map[string]TestRequestT)

	for i, request := range testCaseGroup.Requests {
		if update && !forceUpdate && !request.NeedSave {
			responses[i] = request
			continue
		}

		// replace params in template url but save original url before
		if firstBody != nil {
			requestURLOriginal = request.URL
			request.URL = replaceParams(request.URL, firstBody)
		}
		// do request and get response
		response, respTime, err := getTestCaseResponse(&request)
		if err != nil {
			fmt.Println(err)
			request.Errors = append(request.Errors, err)
			responses[i] = request
			continue
		}
		rawBody := []byte(response.Body())
		if firstBody == nil {
			firstBody = rawBody
		} else {
			request.URL = requestURLOriginal
		}
		request.RawBody = rawBody
		request.Code = response.StatusCode()
		request.Body = responseToHash(request)
		request.Time = respTime
		responses[i] = request
	}

	channel <- TestCaseGroupT{
		Description: testCaseGroup.Description,
		Requests:    responses,
	}
}

// Get slice of bytes and return hash
func getHash(str []byte) string {
	hash := xxhash.New()
	_, err := hash.Write([]byte(str))
	if err != nil {
		fmt.Println("Cannot write hash")
		fmt.Println(err)
		os.Exit(1)
	}
	return fmt.Sprintf("%x", hash.Sum(nil))
}

// Hashes response
func responseToHash(resp TestRequestT) string {
	// Collect strings for hashing
	var err error
	responseBody, err := resp.RawBody, nil

	// Deleted values of fields that is specified into IgnoreFields field of request
	for _, field := range resp.IgnoreFields {
		var path []string
		emptyValue := []byte("")
		fieldNames := strings.Split(field, ".")
		for _, field := range fieldNames {
			path = append(path, field)
		}

		// Replaced value with empty value
		responseBody, err = jsonparser.Set(responseBody, emptyValue, path...)

		if err != nil {
			fmt.Println("Cannot replaced value with empty value. Path: ", path)
			fmt.Println(err)
		}
	}
	return getHash(responseBody)
}

// Save test case
func saveTestCases(responseGroups []TestCaseGroupT) {
	var testCases testCasesT

	fileResponses, err := os.OpenFile("TestCases.lock", os.O_WRONLY, 6)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
	for _, testCaseGroup := range responseGroups {
		testCases.TestCase = append(testCases.TestCase, testCaseGroup)
	}
	responsesEncoder := toml.NewEncoder(fileResponses)
	_ = responsesEncoder.Encode(testCases)

}

// Check test case
func checkTestCase(req TestRequestT, resp TestRequestT, channel chan TestRequestT) {
	if req.Body != resp.Body {
		err := errors.New("the body of response isn't correctly")
		resp.Errors = append(resp.Errors, err)
	}
	if req.Code != resp.Code {
		err := errors.New("the code of response isn't correctly")
		resp.Errors = append(resp.Errors, err)
	}
	channel <- resp
}

func main() {
	// declare flags
	forceUpdate := flag.Bool(
		"update-all", false, "Resave all test cases.")
	softUpdate := flag.Bool(
		"update",
		false,
		"Resave only test cases with parameter needSave equal true.",
	)
	flag.Parse()

	// general flag for update
	update := *forceUpdate || *softUpdate

	// read file witch test cases
	testCasesFile, _ := os.Open("TestCases.lock")
	testCases, err := getTestCases(testCasesFile)
	if err != nil {
		fmt.Println("Error load test cases:")
		fmt.Println(err)
		os.Exit(1)
	}

	respChannel := make(chan TestCaseGroupT, len(testCases))

	for i := range testCases {
		go doTestCaseGroup(&testCases[i], respChannel, update, *forceUpdate)
	}
	// run update test cases
	if update {
		var results []TestCaseGroupT
		for range testCases {
			results = append(results, <-respChannel)
		}
		saveTestCases(results)
		os.Exit(0)
	}

	// run check test cases
	checkChannel := make(chan TestRequestT, len(testCases))
	for i := range testCases {
		respGroup := <-respChannel
		for j, resp := range respGroup.Requests {
			// skip check test case if response errors or needSave equal true
			if len(resp.Errors) != 0 || resp.NeedSave {
				checkChannel <- resp
				continue
			}
			fmt.Println(j, resp.Errors == nil, resp.NeedSave)
			go checkTestCase(testCases[i].Requests[j], resp, checkChannel)
		}

	}

	// collect results test cases runs
	var results []TestRequestT
	for range testCases {
		results = append(results, <-checkChannel)
	}

	// do something with the results
	for _, result := range results {
		// skip if needSave equal true
		if result.NeedSave {
			continue
		}
		// print error if exist
		if result.Errors != nil {
			for _, err := range result.Errors {
				fmt.Println("Result has Errors")
				fmt.Println(err)
			}
			continue
		}
		fmt.Println("ResponseCode= ", result.Code)
		fmt.Println("ResponseBody= ", result.Body)
		fmt.Println("ResponseTime= ", result.Time)
	}
}
