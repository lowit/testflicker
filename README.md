# testflicker
_Another framework for testing HTTP API_

[![pipeline status](https://gitlab.com/lowit/testflicker/badges/master/pipeline.svg)](https://gitlab.com/lowit/testflicker/commits/master)
[![coverage report](https://gitlab.com/lowit/testflicker/badges/master/coverage.svg)](https://gitlab.com/lowit/testflicker/commits/master)

## Run checker:
1. Create `TestCases.lock` in toml format in project directory. (See `TestCases.lock.example`).
    ```
    vim TestCases.lock
    ```
    _or_
    ```
    cp TestCases.lock.example TestCases.lock && vim TestCases.lock
    ```
1. Run.  
    _binary file:_
    ```
    ./testflicker
    ```
    _or sources:_
    ```
    go run testflicker.go
    ```
    
## Build:
```
go build testflicker.go
```


## Run test server:

    go run instruments/test_server.go